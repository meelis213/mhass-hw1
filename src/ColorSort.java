import java.util.Arrays;


public class ColorSort {

	enum Color {
		red, green, blue
	};

	public static void main(String[] param) {
		Color[] colors = {Color.blue,Color.blue,Color.blue,Color.blue};
		reorder(colors);
		System.out.println(Arrays.asList(colors));
		
	}


	
	public static void reorder(Color[] balls) {
		for (int i = 0; i < balls.length - 1; i++) {
			for (int j = i + 1; j < balls.length; j++) {
				Color color1 = balls[i];	// Takes out the ball
				Color color2 = balls[j];
				if (color1.ordinal() > color2.ordinal()) { // Compare color indexes in the enumeration
					balls[i] = color2;	// Flip colors
					balls[j] = color1;
				}
			}
		}
	}
}
